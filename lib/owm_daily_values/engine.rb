# frozen_string_literal: true

module OwmDailyValues
  class Engine < ::Rails::Engine
    isolate_namespace OwmDailyValues
    config.generators.api_only = true

    DEFAULT_STYLES = { horizontal_align: 'center' }.freeze
  end
end
