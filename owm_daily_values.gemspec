# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'owm_daily_values/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'owm_daily_values'
  spec.version     = OwmDailyValues::VERSION
  spec.authors     = ['Tobias Grasse']
  spec.email       = ['tg@glancr.de']
  spec.homepage    = ''
  spec.summary     = 'mirr.OS widget for daily min/max temperatures and sunset/rise times.'
  spec.description = 'Shows min/max temperatures for the day, and when the sun will rise or set.'
  spec.license     = 'MIT'
  spec.metadata    = { 'json' =>
    {
      type: 'widgets',
      title: {
        enGb: 'Daily Values',
        deDe: 'Tageswerte'
      },
      description: {
        enGb: spec.description,
        deDe: 'Zeigt die Tagestiefst- und -höchsttemperatur sowie den Sonnenauf- und -untergang.',
        frFr: 'Affiche les températures minimales/maximales pour la journée et le lever ou le coucher du soleil.',
        esEs: 'Muestra temperaturas mínimas/máximas para el día y cuándo saldrá o se pondrá el sol.',
        plPl: 'Pokazuje minimalne/maksymalne temperatury w ciągu dnia oraz kiedy słońce wschodzi lub zachodzi.',
        koKr: '하루의 최소 / 최대 온도와 해가 뜨거나지는시기를 나타냅니다.'
      },
      group: 'weather_owm',
      compatibility: '1.0.2',
      sizes: [
        { w: 2, h: 1 }
      ],
      single_source: false
    }.to_json }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_development_dependency 'rails'
  spec.add_development_dependency 'rubocop', '~> 0.81'
  spec.add_development_dependency 'rubocop-rails'
end
